/**
 * @file knitroProblem.cpp
 * 
 * @brief File containing implementation of problem class used by Knitro.
 *
 * ==============================================================================\n
 * © Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
 * ==============================================================================\n
 *
 *
 * @author Dominik Bongartz, Jaromil Najman, Alexander Mitsos
 * @date 26.09.2018
 * 
 */

#include <cassert>
#include <string>
#include "KTRException.h"
#include "knitroProblem.h"


using namespace knitro;


/////////////////////////////////////////////////////////////////////////
// constructor
KnitroProblem::KnitroProblem():
KTRProblem(1,0,0)
{	

	setObjType(knitro::KTREnums::ObjectiveType::ObjGeneral);
	setVarTypes(0, knitro::KTREnums::VariableType::Continuous);
	
}


/////////////////////////////////////////////////////////////////////////
// returns the value of the objective function and constraints
double KnitroProblem::evaluateFC(const double* const x, double* const c, double* const objGrad, double* const jac) 
{
  	return x[0];
}


/////////////////////////////////////////////////////////////////////////
// return the gradient of the objective function and constraints
int KnitroProblem::evaluateGA(const double* const x, double* const objGrad, double* const jac)
{
	
	objGrad[0] = 1;		
	
	return 0;
}

