/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This file is made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include <string>
#include <iostream>

#include "knitroProblem.h"

#include "KTRException.h"
#include "KTRSolver.h"


int main() {

	KnitroProblem* myKnitroProblem = new KnitroProblem;
	knitro::KTRSolver myKnitro(myKnitroProblem, KN_GRADOPT_EXACT, KN_HESSOPT_BFGS);
	delete myKnitroProblem;

#ifdef HAVE_KNITRO
	std::cout << "Preprocessor variable HAVE_KNITRO was set correctly." << std::endl;
	std::cout << "Knitro seems to work..." << std::endl;
	std::cout << "Note that this is merely intended to test the repository / CMake setup and not functionality..." << std::endl;
#else
	std::cout << "Something went wrong, preprocessor variable HAVE_KNITRO was NOT set correctly." << std::endl;
#endif

	return 0;

}
