/**
 * @file knitroProblem.h
 * 
 * @brief File containing declaration of problem class used by Knitro.
 *
 * ==============================================================================\n
 * © Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
 * ==============================================================================\n
 *
 *
 * @author Dominik Bongartz, Jaromil Najman, Alexander Mitsos
 * @date 26.09.2018
 * 
 */

#pragma once

#include "KTRException.h"
#include "KTRProblem.h"
#include "KTRSolver.h"

using namespace knitro;

	
	
/**
* @class KnitroProblem
* @brief Class for representing problems to be solved by Knitro, providing an interface to the problem definition in problem.h 
* 
* This class provides an interface between Knitro and the problem definition in problem.h. by evaluating the Model equations and preparing the information required by Knitro.
* An instance of this class is handed to Knitro as an argument of its KN_solve routine in ubpKnitro.cpp.
* For more information on the basic interface see  https://www.artelys.com/tools/knitro_doc/3_referenceManual/callableLibraryAPI.html#basic-problem-construction .
*
*/
class KnitroProblem : public KTRProblem
{
	public:
	
	
		/** 
		* @brief Constructor actually used in ubp.cpp. Initializes the corresponding members.
		*/
		KnitroProblem();

		/** @brief Destructor */
		virtual ~KnitroProblem(){}
		
		/** 
		* @brief Function called by Knitro to get values of the objective and constraints at a point x
		*
		* @param[in] x is the current point
		* @param[out] c is the value of constraints
		* @param[out] objGrad is the gradient of the objective at x (not set in this function)
		* @param[out] jac holds the Jacobian values at x (not set in this function)
		* @return Returns the value of the objective function at x
		*/
		double evaluateFC(const double* const x, double* const c, double* const objGrad, double* const jac);

		/** 
		* @brief Function called by Knitro to get derivatives of the objective and constraints at point x
		* 
		* @param[in] x is the current point
		* @param[out] objGrad is the gradient of the objective at x 
		* @param[out] jac holds the Jacobian values at x
		* @return Returns the value of the gradients of the objective function at x
		*/
		int evaluateGA(const double* const x, double* const objGrad, double* const jac);		
		
		
	private:
		
	
	  
	  // Copy constructors made private 
	  KnitroProblem(const KnitroProblem&);						/*!< default copy constructor declared private to prevent use */		
	  KnitroProblem& operator=(const KnitroProblem&);				/*!< default assignment operator declared private to prevent use */

};
